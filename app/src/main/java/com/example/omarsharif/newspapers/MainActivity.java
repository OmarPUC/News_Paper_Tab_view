package com.example.omarsharif.newspapers;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        setContentView(R.layout.tab1);
////        setContentView(R.layout.tab2);
////        setContentView(R.layout.tab3);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    Tab1 tab1 = new Tab1();
                    return tab1;
                case 1:
                    Tab2 tab2 = new Tab2();
                    return tab2;
                case 2:
                    Tab3 tab3 = new Tab3();
                    return tab3;
            }
            return null;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }
    }

    public void goToLink(View v) {
        Intent intent = new Intent(this, Main2Activity.class);
        switch (v.getId()) {
            case R.id.imageButton1:
                intent.putExtra("url", "http://www.prothomalo.com");
                startActivity(intent);
                break;
            case R.id.imageButton2:
                intent.putExtra("url", "http://www.bd-pratidin.com");
                startActivity(intent);
                break;
            case R.id.imageButton3:
                intent.putExtra("url", "http://www.dailynayadiganta.com");
                startActivity(intent);
                break;
            case R.id.imageButton4:
                intent.putExtra("url", "http://www.ittefaq.com.bd");
                startActivity(intent);
                break;
            case R.id.imageButton5:
                intent.putExtra("url", "https://www.bbc.com/bengali");
                startActivity(intent);
                break;
            case R.id.imageButton6:
                intent.putExtra("url", "http://bonikbarta.net/bangla");
                startActivity(intent);
                break;
            case R.id.imageButton7:
                intent.putExtra("url", "http://www.kalerkantho.com");
                startActivity(intent);
                break;
            case R.id.imageButton8:
                intent.putExtra("url", "http://www.mzamin.com");
                startActivity(intent);
                break;
            case R.id.imageButton9:
                intent.putExtra("url", "https://www.jugantor.com");
                startActivity(intent);
                break;
            case R.id.imageButton10:
                intent.putExtra("url", "http://www.samakal.com");
                startActivity(intent);
                break;
            case R.id.imageButton11:
                intent.putExtra("url", "https://www.thedailystar.net");
                startActivity(intent);
                break;
            case R.id.imageButton12:
                intent.putExtra("url", "http://www.daily-sun.com");
                startActivity(intent);
                break;
            case R.id.imageButton13:
                intent.putExtra("url", "http://www.theindependentbd.com");
                startActivity(intent);
                break;
            case R.id.imageButton14:
                intent.putExtra("url", "http://www.thedailynewnation.com");
                startActivity(intent);
                break;
            case R.id.imageButton15:
                intent.putExtra("url", "http://www.bbc.com");
                startActivity(intent);
                break;
            case R.id.imageButton16:
                intent.putExtra("url", "https://www.aljazeera.com");
                startActivity(intent);
                break;
            case R.id.imageButton17:
                intent.putExtra("url", "http://www.newagebd.net");
                startActivity(intent);
                break;
            case R.id.imageButton18:
                intent.putExtra("url", "https://edition.cnn.com");
                startActivity(intent);
                break;
            case R.id.imageButton19:
                intent.putExtra("url", "http://www.observerbd.com");
                startActivity(intent);
                break;
            case R.id.imageButton20:
                intent.putExtra("url", "http://thebangladeshtoday.com");
                startActivity(intent);
                break;
            case R.id.imageButton21:
                intent.putExtra("url", "https://sports.yahoo.com");
                startActivity(intent);
                break;
            case R.id.imageButton22:
                intent.putExtra("url", "http://www.espn.in");
                startActivity(intent);
                break;
            case R.id.imageButton23:
                intent.putExtra("url", "https://www.bleacherreport.com");
                startActivity(intent);
                break;
            case R.id.imageButton24:
                intent.putExtra("url", "https://www.nbcsports.com");
                startActivity(intent);
                break;
            case R.id.imageButton25:
                intent.putExtra("url", "https://www.cricbuzz.com");
                startActivity(intent);
                break;
            case R.id.imageButton26:
                intent.putExtra("url", "http://www.marca.com/en");
                startActivity(intent);
                break;
            case R.id.imageButton27:
                intent.putExtra("url", "https://www.sport-english.com/en");
                startActivity(intent);
                break;
            case R.id.imageButton28:
                intent.putExtra("url", "http://www.espncricinfo.com");
                startActivity(intent);
                break;
            case R.id.imageButton29:
                intent.putExtra("url", "https://www.cricketworld.com");
                startActivity(intent);
                break;
            case R.id.imageButton30:
                intent.putExtra("url", "http://www.skysports.com");
                startActivity(intent);
                break;
            case R.id.imageButton31:
                intent.putExtra("url", "http://www.jaijaidinbd.com");
                startActivity(intent);
                break;
            case R.id.imageButton34:
                intent.putExtra("url", "http://www.dailysangram.com");
                startActivity(intent);
                break;
            case R.id.imageButton32:
                intent.putExtra("url", "https://www.dailyinqilab.com");
                startActivity(intent);
                break;
            case R.id.imageButton33:
                intent.putExtra("url", "http://www.bhorerkagoj.com");
                startActivity(intent);
                break;
            case R.id.imageButton:
                intent.putExtra("url", "https://timesofindia.indiatimes.com");
                startActivity(intent);
                break;
            case R.id.imageButton35:
                intent.putExtra("url", "https://www.nytimes.com");
                startActivity(intent);
                break;
            case R.id.imageButton36:
                intent.putExtra("url", "https://www.express.co.uk");
                startActivity(intent);
                break;
            case R.id.imageButton38:
                intent.putExtra("url", "https://www.telegraph.co.uk");
                startActivity(intent);
                break;
            case R.id.imageButton40:
                intent.putExtra("url", "https://www.cricket365.com");
                startActivity(intent);
                break;
            case R.id.imageButton37:
                intent.putExtra("url", "https://www.sportinglife.com");
                startActivity(intent);
                break;
            case R.id.imageButton41:
                intent.putExtra("url", "https://www.fifa.com/?nav=internal");
                startActivity(intent);
                break;
            case R.id.imageButton39:
                intent.putExtra("url", "https://www.icc-cricket.com");
                startActivity(intent);
                break;
        }
    }
}
